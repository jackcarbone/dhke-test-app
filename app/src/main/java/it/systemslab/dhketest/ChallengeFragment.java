package it.systemslab.dhketest;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import it.systemslab.cryptomodule.Credentials;
import it.systemslab.cryptomodule.DHKEInstance;
import it.systemslab.dhketest.rest.DHKERequest;
import it.systemslab.dhketest.rest.DHKERest;
import it.systemslab.dhketest.rest.DHKERestListener;

public class ChallengeFragment extends Fragment implements DHKERestListener {
	private static final String TAG = ChallengeFragment.class.getSimpleName();
	private TextView challenge;
	private TextView solved;
	private Credentials credentials;
	private String sharedSymmetrickey;
	private String remotePublicKey;


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		View v = inflater.inflate(R.layout.fragment_challenge, container, false);
		challenge = v.findViewById(R.id.challenge);
		solved = v.findViewById(R.id.solved);
		v.findViewById(R.id.start).setOnClickListener(this::start);

		MainActivity main = ((MainActivity) getActivity());
		if(main != null) {
			credentials = main.getCredentials();
			remotePublicKey = ((MainActivity) getActivity()).getRemotePublicKey();
			sharedSymmetrickey = ((MainActivity) getActivity()).getSharedSymmetricKey();
		} else {
			Toast.makeText(getActivity(), "No main", Toast.LENGTH_SHORT).show();
		}

		return v;
	}

	boolean isReady() {
		if(credentials == null)
			return false;
		if(credentials.getPrivateKey() == null || credentials.getPublicKey() == null)
			return false;
		if(remotePublicKey == null)
			return false;
		return sharedSymmetrickey != null;
	}

	public void start(View view) {
		if(isReady())
			new Thread(() -> new DHKERest(getActivity()).startChallenge((DHKERestListener) this)).start();
		else
			Toast.makeText(getActivity(), "Completa i passaggi precedenti", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSuccess(String message) {

		challenge.setText(message);
		String decrypted = DHKEInstance.getInstance()
				.decryptMessage(sharedSymmetrickey, message);
		solved.setText(decrypted);

		DHKERequest DHKERequest = new DHKERequest();
		DHKERequest.setMessage(decrypted);
		new Thread(() -> new DHKERest(getActivity()).resolveChallenge(DHKERequest, (DHKERestListener) this)).start();
	}

	@Override
	public void onFailure(String error) {
		Log.d(TAG, "onFailure: " + error);
		Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onResolveChallenge(String message) {
		Toast.makeText(getActivity(), "Challenge: " + message, Toast.LENGTH_SHORT).show();
	}

}

package it.systemslab.dhketest;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import it.systemslab.cryptomodule.Credentials;
import it.systemslab.cryptomodule.DHKEInstance;
import it.systemslab.dhketest.fragment.GenerateFragment;
import it.systemslab.dhketest.fragment.SendFragment;
import it.systemslab.dhketest.fragment.SwapFragment;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ViewPager container;
    private BottomNavigationView bottom;
    private static Credentials credentials;    /// coppia chiave pubblica e chiave privata del client
    private String remotePublicKey;     /// chiave pubblica del server
    private String sharedSymmetricKey;  /// chiave simmetrica condivisa per cifratura e decifratura

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_main);

        credentials = new Credentials(); // evitiamo null pointer?

        bottom = findViewById(R.id.bottom);
        bottom.setOnNavigationItemSelectedListener(this);
        container = findViewById(R.id.container);
        container.setAdapter(new DHKEPageAdapter(getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT));
        container.addOnPageChangeListener(this);
    }

    public Credentials getCredentials() {
        Log.d(TAG, "getCredentials: private: " + credentials.getPrivateKey());
        Log.d(TAG, "getCredentials: public: " + credentials.getPublicKey());
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public String getSharedSymmetricKey() {
        return sharedSymmetricKey;
    }

    public void setSharedSymmetricKey(String sharedSymmetricKey) {
        this.sharedSymmetricKey = sharedSymmetricKey;
    }

    public String getRemotePublicKey() {
        return remotePublicKey;
    }

    public void setRemotePublicKey(String remotePublicKey) {
        this.remotePublicKey = remotePublicKey;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.genera:
                container.setCurrentItem(0, true);
                break;
            case R.id.scambia:
                container.setCurrentItem(1, true);
                break;
            case R.id.invia:
                container.setCurrentItem(2, true);
                break;
            case R.id.challenge:
                container.setCurrentItem(3, true);
                break;
        }

        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "onPageSelected: " + position);
        switch (position) {
            case 0:
                bottom.setSelectedItemId(R.id.genera);
                break;
            case 1:
                bottom.setSelectedItemId(R.id.scambia);
                break;
            case 2:
                bottom.setSelectedItemId(R.id.invia);
                break;
            case 3:
                bottom.setSelectedItemId(R.id.challenge);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d(TAG, "onPageScrollStateChanged: " + state);
    }

    private static class DHKEPageAdapter extends FragmentStatePagerAdapter {
        public DHKEPageAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new GenerateFragment();
                case 1: {
                    Log.d(TAG, "getItem: SwapFragment");
                    SwapFragment swapFragment = new SwapFragment();
                    swapFragment.setCredentials(credentials);
                    return new SwapFragment();
                }
                case 2:
                    return new SendFragment();
                case 3:
                    return new ChallengeFragment();
                default:
                    return new Fragment();
            }
        }
    }
}
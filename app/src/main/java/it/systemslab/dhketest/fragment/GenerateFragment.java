package it.systemslab.dhketest.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import it.systemslab.cryptomodule.Credentials;
import it.systemslab.cryptomodule.DHKEInstance;
import it.systemslab.dhketest.MainActivity;
import it.systemslab.dhketest.R;

public class GenerateFragment extends Fragment {

  private TextView publicKey;
  private TextView privateKey;
  private Credentials credentials;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    View v = inflater.inflate(R.layout.fragment_generate, container, false);
    Button b = v.findViewById(R.id.genera);
    privateKey = v.findViewById(R.id.private_key);
    publicKey = v.findViewById(R.id.public_key);

    b.setOnClickListener(this::genera);

    credentials = ((MainActivity) getActivity()).getCredentials();
    if(credentials != null && credentials.getPublicKey() != null && credentials.getPrivateKey() != null) {
      privateKey.setText(credentials.getPrivateKey());
      publicKey.setText(credentials.getPublicKey());
    }

    return v;
  }

  public void genera(View view) {

    String pubKey;
    String privKey;

    try {
      Credentials credentials = DHKEInstance.getInstance().generateCredentials();
      pubKey = credentials.getPublicKey();
      privKey = credentials.getPrivateKey();

      ((MainActivity) getActivity()).setCredentials(credentials);
      this.credentials = credentials;
    } catch (NoSuchAlgorithmException e) {
      pubKey = String.format(Locale.ENGLISH, "NoSuchAlgorithmException: %s", e.toString());
      privKey = String.format(Locale.ENGLISH, "NoSuchAlgorithmException: %s", e.toString());
      e.printStackTrace();
    } catch (InvalidAlgorithmParameterException e) {
      pubKey = String.format(Locale.ENGLISH, "InvalidAlgorithmParameterException: %s", e.toString());
      privKey = String.format(Locale.ENGLISH, "InvalidAlgorithmParameterException: %s", e.toString());
      e.printStackTrace();
    }

    publicKey.setText(pubKey);
    privateKey.setText(privKey);
  }
}

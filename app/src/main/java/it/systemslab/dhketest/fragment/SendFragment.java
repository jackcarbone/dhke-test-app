package it.systemslab.dhketest.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import it.systemslab.cryptomodule.Credentials;
import it.systemslab.cryptomodule.DHKEInstance;
import it.systemslab.dhketest.MainActivity;
import it.systemslab.dhketest.R;
import it.systemslab.dhketest.rest.DHKERequest;
import it.systemslab.dhketest.rest.DHKERest;
import it.systemslab.dhketest.rest.DHKERestListener;

public class SendFragment extends Fragment implements DHKERestListener {

private static final String TAG = SendFragment.class.getSimpleName();
private EditText messageToEncrypt;
	private TextView cifrato;
	private TextView decrypted;
	private Credentials credentials;
	private String sharedSymmetrickey;
	private String remotePublicKey;


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View v = inflater.inflate(R.layout.fragment_send, container, false);
		v.findViewById(R.id.invia).setOnClickListener(this::invia);
		messageToEncrypt = v.findViewById(R.id.messageToEncrypt);
		cifrato = v.findViewById(R.id.cifrato);
		decrypted = v.findViewById(R.id.decrypted);

		MainActivity main = ((MainActivity) getActivity());
		if(main != null) {
			credentials = main.getCredentials();
			remotePublicKey = ((MainActivity) getActivity()).getRemotePublicKey();
			sharedSymmetrickey = ((MainActivity) getActivity()).getSharedSymmetricKey();
		} else {
			Toast.makeText(getActivity(), "No main", Toast.LENGTH_SHORT).show();
		}

		return v;
	}

	boolean isReady() {
		if(credentials == null)
			return false;
		if(credentials.getPrivateKey() == null || credentials.getPublicKey() == null)
			return false;
		if(remotePublicKey == null)
			return false;
		return sharedSymmetrickey != null;
	}

	public void invia(View view) {
		if(!isReady()) {
			Toast.makeText(getActivity(), "Completa i passaggi precedenti", Toast.LENGTH_SHORT).show();
			return;
		}

		String toEncrypt = messageToEncrypt.getText().toString();
		if(toEncrypt.length() == 0) {
			Toast.makeText(getActivity(), "Inserire un messaggio da cifrare", Toast.LENGTH_SHORT).show();
			return;
		}

		String encrypted = DHKEInstance.getInstance()
				.encryptMessage(sharedSymmetrickey, toEncrypt);
		if(encrypted != null) {
			cifrato.setText(encrypted);
			DHKERequest DHKERequest = new DHKERequest();
			DHKERequest.setMessage(encrypted);
			new Thread(() -> new DHKERest(getActivity()).encrypted(DHKERequest, (DHKERestListener) this)).start();
		} else {
			Toast.makeText(getActivity(), "Errore in fase di cifratura", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onSuccess(String message) {
		Log.d(TAG, "onSuccess: " + message);
		decrypted.setText(message);
		if(message != null && message.equals(messageToEncrypt.getText().toString()))
			Toast.makeText(getActivity(), "Successo!", Toast.LENGTH_SHORT).show();
		else {
			Toast.makeText(getActivity(), "I due risultati non coincidono", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onFailure(String error) {
		Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onResolveChallenge(String message) {
		Log.d(TAG, "onResolveChallenge: " + message);
	}
}

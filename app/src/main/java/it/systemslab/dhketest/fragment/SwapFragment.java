package it.systemslab.dhketest.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import it.systemslab.cryptomodule.Credentials;
import it.systemslab.cryptomodule.DHKEInstance;
import it.systemslab.dhketest.MainActivity;
import it.systemslab.dhketest.R;
import it.systemslab.dhketest.rest.DHKERest;
import it.systemslab.dhketest.rest.DHKERestListener;
import it.systemslab.dhketest.rest.ExchangeRequest;

public class SwapFragment extends Fragment implements DHKERestListener {

    private static final String TAG = SwapFragment.class.getSimpleName();
    private TextView remotePublic;
    private TextView commonSecret;
    private Credentials credentials;
    private String sharedSymmetrickey;
    private String remotePublicKey;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_swap, container, false);
        v.findViewById(R.id.scambia).setOnClickListener(this::scambia);
        remotePublic = v.findViewById(R.id.remote_public);
        commonSecret = v.findViewById(R.id.common_secret);

        MainActivity main = ((MainActivity) getActivity());
        if(main != null) {
            // credentials = main.getCredentials();
            remotePublicKey = ((MainActivity) getActivity()).getRemotePublicKey();
            sharedSymmetrickey = ((MainActivity) getActivity()).getSharedSymmetricKey();
            if (isReady()) {
                remotePublic.setText(remotePublicKey);
                commonSecret.setText(sharedSymmetrickey);
            }
        } else {
            remotePublic.setText("no main");
            commonSecret.setText("no main");
        }

        return v;
    }

    boolean isReady() {
        if(credentials == null)
            return false;
        if(credentials.getPrivateKey() == null || credentials.getPublicKey() == null)
            return false;
        if(remotePublicKey == null)
            return false;
        return sharedSymmetrickey != null;
    }

    public void scambia(View view) {
        if(credentials == null) {
            Toast.makeText(getActivity(), "no credentials", Toast.LENGTH_SHORT).show();
            return;
        }

        Log.d(TAG, "scambia: private: " + credentials.getPrivateKey());
        Log.d(TAG, "scambia: public: " + credentials.getPublicKey());
        if(credentials.getPrivateKey() == null || credentials.getPublicKey() == null) {
            Toast.makeText(getActivity(), "Generare le chiavi", Toast.LENGTH_SHORT).show();
            return;
        }

        ExchangeRequest exchange = new ExchangeRequest();
        exchange.setPublicKey(credentials.getPublicKey());
        new Thread(() -> new DHKERest(getActivity()).exchange(exchange, (DHKERestListener) this)).start();
    }

    @Override
    public void onSuccess(String remotePK) {

        if(credentials == null) {
            Toast.makeText(getContext(), "no credentials", Toast.LENGTH_SHORT).show();
            return;
        }
        MainActivity main = (MainActivity) getActivity();
        if(main == null) {
            Toast.makeText(getContext(), "no main", Toast.LENGTH_SHORT).show();
            return;
        }

        String error = "Errore!";
        try {
            remotePublicKey = remotePK;
            sharedSymmetrickey = DHKEInstance.getInstance().generateSharedSymmetricKey(
                    credentials.getPublicKey(),
                    remotePK
            );

            remotePublic.setText(remotePublicKey);
            commonSecret.setText(sharedSymmetrickey);

            return;
        } catch (InvalidKeySpecException e) {
            error = "[InvalidKeySpecException] exception: " + e.toString();
            System.out.println(error);
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            error = "[NoSuchAlgorithmException] exception: " + e.toString();
            System.out.println(error);
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            error = "[InvalidKeyException] exception: " + e.toString();
            System.out.println(error);
            e.printStackTrace();
        }

        remotePublic.setText(error);
        commonSecret.setText(error);

    }

    @Override
    public void onFailure(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResolveChallenge(String message) {

    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }
}

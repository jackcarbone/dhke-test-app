package it.systemslab.dhketest.rest;

public class DHKERequest {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

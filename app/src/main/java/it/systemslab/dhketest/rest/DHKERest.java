package it.systemslab.dhketest.rest;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import java.util.Objects;

import it.systemslab.cryptomodule.HashablePrivateData;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DHKERest {

    private final static String TAG = "DHKERest";
    /// private final static String BASE_URL = "https://dhke-test.herokuapp.com";
    private final static String BASE_URL = "http://10.0.0.44:8084";

    private final static String EXCHANGE = BASE_URL + "/receive/publickey";
    private final static String ENCRYPTED = BASE_URL + "/receive/encrypted-message";
    private final static String START_CHALLENGE = BASE_URL + "/challenge/start";
    private final static String RESOLVE_CHALLENGE = BASE_URL + "/challenge/resolve";
    private final static String CREATE_HASH = BASE_URL + "/hash/create";
    private final static String VALIDATE_HASH = BASE_URL + "/hash/validate";

    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private final Activity context;

    private OkHttpClient client = new OkHttpClient();

    public DHKERest(Activity context) {
        this.context = context;
    }

    public void exchange(ExchangeRequest exchangeRequest, DHKERestListener listener) {
        RequestBody body = RequestBody.create(
                new Gson().toJson(exchangeRequest, ExchangeRequest.class),
                JSON);

        Request request = new Request.Builder()
                .url(EXCHANGE)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            String b = Objects.requireNonNull(response.body()).string();
            Log.d(TAG, "exchange: " + b);
            DHKEResponse dhkeResponse = new Gson().fromJson(b, DHKEResponse.class);
            if(dhkeResponse.code == 200)
                context.runOnUiThread(() -> listener.onSuccess(dhkeResponse.message));
            else
                context.runOnUiThread(() -> listener.onFailure("Exchange error: " + dhkeResponse.message));
        } catch (Exception e) {
            Log.d(TAG, "exchange: e: " + e.toString());
            context.runOnUiThread(() -> listener.onFailure("Exchange error: " + e.toString()));
        }
    }

    public void encrypted(DHKERequest DHKERequest, DHKERestListener listener) {
        RequestBody body = RequestBody.create(
                new Gson().toJson(DHKERequest, DHKERequest.class),
                JSON);

        Request request = new Request.Builder()
                .url(ENCRYPTED)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            String b = Objects.requireNonNull(response.body()).string();
            Log.d(TAG, "Encrypted: " + b);
            DHKEResponse dhkeResponse = new Gson().fromJson(b, DHKEResponse.class);
            if(dhkeResponse.code == 200)
                context.runOnUiThread(() -> listener.onSuccess(dhkeResponse.message));
            else
                context.runOnUiThread(() -> listener.onFailure("Encrypted error: " + dhkeResponse.message));
        } catch (Exception e) {
            Log.d(TAG, "Encrypted: e: " + e.toString());
            context.runOnUiThread(() -> listener.onFailure("Encrypted error: " + e.toString()));
        }
    }

    public void startChallenge(DHKERestListener listener) {

        Request request = new Request.Builder()
                .url(START_CHALLENGE)
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            String b = Objects.requireNonNull(response.body()).string();
            Log.d(TAG, "startChallenge: " + b);
            DHKEResponse dhkeResponse = new Gson().fromJson(b, DHKEResponse.class);
            if(dhkeResponse.code == 200)
                context.runOnUiThread(() -> listener.onSuccess(dhkeResponse.message));
            else
                context.runOnUiThread(() -> listener.onFailure("startChallenge error: " + dhkeResponse.message));
        } catch (Exception e) {
            Log.d(TAG, "startChallenge: e: " + e.toString());
            context.runOnUiThread(() -> listener.onFailure("startChallenge error: " + e.toString()));
        }
    }


    public void resolveChallenge(DHKERequest DHKERequest, DHKERestListener listener) {
        RequestBody body = RequestBody.create(
          new Gson().toJson(DHKERequest, DHKERequest.class),
          JSON);

        Request request = new Request.Builder()
          .url(RESOLVE_CHALLENGE)
          .post(body)
          .build();
        try (Response response = client.newCall(request).execute()) {
            String b = Objects.requireNonNull(response.body()).string();
            Log.d(TAG, "resolveChallenge: " + b);
            DHKEResponse dhkeResponse = new Gson().fromJson(b, DHKEResponse.class);

            context.runOnUiThread(() -> listener.onResolveChallenge(dhkeResponse.message));
        } catch (Exception e) {
            Log.d(TAG, "resolveChallenge: e: " + e.toString());
            context.runOnUiThread(() -> listener.onFailure("resolveChallenge error: " + e.toString()));
        }
    }

    public void createHash(HashablePrivateData data, DHKERestListener listener) {
        RequestBody body = RequestBody.create(
            new Gson().toJson(data, HashablePrivateData.class),
            JSON
        );

        Request request= new Request.Builder()
            .url(CREATE_HASH)
            .post(body)
            .build();
        try(Response response = client.newCall(request).execute()) {
            String b = Objects.requireNonNull(response.body()).string();
            Log.d(TAG, "resolveChallenge: " + b);

            context.runOnUiThread(() -> listener.onResolveChallenge(b));
        } catch (Exception e) {
            Log.d(TAG, "createHash: e: " + e.toString());
            context.runOnUiThread(() -> listener.onFailure("createHash error: " + e.toString()));
        }
    }
}

package it.systemslab.dhketest.rest;

public interface DHKERestListener {
    void onSuccess(String remotePK);
    void onFailure(String error);
    void onResolveChallenge(String message);
}

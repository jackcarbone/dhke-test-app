
package it.systemslab.cryptomodule;

import android.os.Build;
import android.util.Log;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static it.systemslab.cryptomodule.Utils.convertPrivateKey;
import static it.systemslab.cryptomodule.Utils.convertPublicKey;
import static it.systemslab.cryptomodule.Utils.fromStringToByte;
import static it.systemslab.cryptomodule.Utils.shortenSecretKey;
import static it.systemslab.cryptomodule.Utils.stringifyArray;


public class DHKEInstance {

    private BigInteger g512 = new BigInteger("153d5d6172adb43045b68ae8e1de1070b6137005686d29d3d73a7749199681ee5b212c9b96bfdcfa5b20cd5e3fd2044895d609cf9b410b7a0f12ca1cb9a428cc", 16);
    private BigInteger p512 = new BigInteger("9494fec095f3b85ee286542b3836fc81a5dd0a0349b4c239dd38744d488cf8e31db8bcb7d33b41abb9e5a33cca9144b1cef332c94bf0573bf047a3aca98cdf3b", 16);

    private static final String TAG = "DHKEInstance";
    private static final String CIPHER = "DES/ECB/PKCS5Padding";

    private DHKEInstance() {
    }

    /**
     * Crea il singoletto
     * @return nuova istanza della classe
     */
    public static DHKEInstance getInstance() {
        return new DHKEInstance();
    }


    /**
     * Genera la coppia {chiave pubblica, chiave privata} come stringhe. Queste stringhe dovranno
     * essere memorizzate in un Room database
     * @return le chiavi pubblica e privata
     * @throws NoSuchAlgorithmException L'algoritmo utilizzato per generare le chiavi non è supportato
     * @throws InvalidAlgorithmParameterException I parametri forniti per la generazione delle chiavi non sono validi
     */
    public Credentials generateCredentials() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        Credentials credentials = new Credentials();

        DHParameterSpec dhParams = new DHParameterSpec(p512, g512);
        final KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DH");
        keyPairGenerator.initialize(dhParams, new SecureRandom());

        final KeyPair keyPair = keyPairGenerator.generateKeyPair();

        credentials.setPrivateKey(stringifyArray(keyPair.getPrivate().getEncoded()));
        credentials.setPublicKey(stringifyArray(keyPair.getPublic().getEncoded()));

        return credentials;
    }

    /**
     * Genera la chiave simmetrica condivisa. Da memorizzare in un Room database.
     * @param localePrivateKey la chiave privata del client ottenuta dal metodo generateCredentials()
     * @param remotePublicKey la chiave pubblica ricevuta dal server remoto
     * @return la chiave simmetrica condivisa in caso di successo; null altrimenti
     * @throws InvalidKeySpecException la chiave non rispetta il formato atteso
     * @throws NoSuchAlgorithmException algoritmo non supportato
     * @throws InvalidKeyException la chiave ricevuta non rispetta il formato atteso
     */
    public String generateSharedSymmetricKey(String localePrivateKey, String remotePublicKey) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException {
        PublicKey remoteKey = convertPublicKey(remotePublicKey);
        PrivateKey privateKey = convertPrivateKey(localePrivateKey);

        final KeyAgreement keyAgreement = KeyAgreement.getInstance("DH");
        keyAgreement.init(privateKey);
        keyAgreement.doPhase(remoteKey, true);

        return stringifyArray(shortenSecretKey(keyAgreement.generateSecret()));
    }

    /**
     * Cifra il messaggio da inviare
     * @param sharedSymmetricKey chiave simmetrica condivisa generata dal metodo generateSharedSymmetricKey()
     * @param message il messaggio da cifrare
     * @return il messaggio cifrato, in caso di successo; null, altrimenti.
     */
    public String encryptMessage(String sharedSymmetricKey, final String message) {

        try {
            // You can use Blowfish or another symmetric algorithm but you must adjust the key size.
            final SecretKeySpec keySpec = new SecretKeySpec(fromStringToByte(sharedSymmetricKey), "DES");
            final Cipher cipher  = Cipher.getInstance(CIPHER);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);

            final byte[] encryptedMessage = cipher.doFinal(message.getBytes());
            System.out.println(
                String.format("encryptAndSend %s %s\n", message, stringifyArray(encryptedMessage))
            );

            return stringifyArray(encryptedMessage);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Decifra il messaggio ricevuto
     * @param sharedSymmetricKey chiave simmetrica condivisa generata dal metodo generateSharedSymmetricKey()
     * @param encryptedMessage il messaggio da decifrare
     * @return il messaggio decifrato, in caso di successo; null, altrimenti.
     */
    public String decryptMessage(String sharedSymmetricKey, final String encryptedMessage) {

        try {

            final SecretKeySpec keySpec = new SecretKeySpec(
                fromStringToByte(sharedSymmetricKey),
                "DES"
            );
            final Cipher cipher = Cipher.getInstance(CIPHER);

            cipher.init(Cipher.DECRYPT_MODE, keySpec);
            final byte[] message = fromStringToByte(encryptedMessage) ;
            String secretMessage = new String(cipher.doFinal(message));

            Log.d(TAG, 
                String.format("receiveAndDecrypt %s %s\n", stringifyArray(message), secretMessage)
            );

            return secretMessage;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Crea l'hash dei dati privati ottenuti dal green pass
     * @param name nome prelevato dal green pass
     * @param surname cognome prelevato dal green pass
     * @param dateOfBirth data di nascita prelevata dal green pass
     * @return l'hash del messaggio
     */
    public String createHash(String name, String surname, String dateOfBirth) {
        HashablePrivateData privateData = new HashablePrivateData(name, surname, dateOfBirth);
        if(!privateData.validateRequestObject())
            throw new IllegalArgumentException("Missing data");

        try {
            System.out.println(
                String.format("[%s] createHash %s", TAG, privateData.toString())
            );
            String originalString = privateData.getHashableString();
            System.out.println(
                String.format("[%s] originalString %s", TAG, originalString)
            );
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return stringifyArray(digest.digest(originalString.getBytes(StandardCharsets.UTF_8)));
            } else {
                return stringifyArray(digest.digest(originalString.getBytes()));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Invalid algorithm SHA-256");
        } catch (Exception e) {
            throw new IllegalArgumentException("/hash/create: unknown error");
        }
    }

    /**
     * Crea l'hash dei dati privati ottenuti dal green pass
     * @param privateData dati prelevati dal green pass
     * @return l'hash del messaggio
     */
    public String createHash(HashablePrivateData privateData) {
        return createHash(
            privateData.getName(), privateData.getSurname(), privateData.getDateOfBirth()
        );
    }

    /**
     * Verifica l'hash dei dati privati ottenuti dal green pass. Non usato per il totem o il watch.
     * @param validateHashRequest contiene l'hash da verificare e i dati per generare l'hash
     * @return true o false se il
     */
    public boolean validateHash(ValidateHashRequest validateHashRequest) {
        if(!validateHashRequest.validateRequestObject())
            throw new IllegalArgumentException("Missing data");
        try {
            System.out.println(
                String.format("[%s] validateHash %s", TAG, validateHashRequest.toString())
            );
            String generatedHash = createHash(validateHashRequest.getPrivateData());
            System.out.println(
                String.format("[%s] generatedHash %s", TAG, generatedHash)
            );
            return generatedHash.equals(validateHashRequest.getHash());
        } catch (Exception e) {
            throw new IllegalArgumentException("/hash/validate: " + e.getMessage());
        }
    }
}

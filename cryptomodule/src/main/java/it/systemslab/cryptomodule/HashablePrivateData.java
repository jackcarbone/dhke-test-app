package it.systemslab.cryptomodule;

public class HashablePrivateData {

  private String name;
  private String surname;
  private String dateOfBirth;

  public HashablePrivateData() {
  }

  public HashablePrivateData(String name, String surname, String dateOfBirth) {
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public boolean validateRequestObject(){
    return this.name != null && this.surname != null && this.dateOfBirth != null;
  }

  public String getHashableString() {
    return String.format("%s;%s;%s;", this.name, this.surname, this.dateOfBirth);
  }

  @Override
  public String toString() {
    return "PrivateData{" +
        "name='" + name + '\'' +
        ", surname='" + surname + '\'' +
        ", dateOfBirth='" + dateOfBirth + '\'' +
        '}';
  }
}

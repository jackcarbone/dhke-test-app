package it.systemslab.cryptomodule;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class Utils {

    /**
     * Restituisce la stringa esadecimale dell'array di byte passato in input
     * @param value
     * @return
     */
    static String stringifyArray(byte[] value) {
        if(value == null)
            return null;

        String s = "";
        for(byte b : value) {
            s = s.concat(String.format("%02x", b));
        }

        return s;
    }

    static byte[] fromStringToByte(String value) {
        byte[] bytes = value.getBytes();
        byte[] result = new byte[value.length() / 2];

        for(int i = 0; i < bytes.length; i++) {
            char[] chars = new char[2];
            chars[0] = (char) bytes[i];
            chars[1] = (char) bytes[i+1];
            int b = Integer.parseInt(new String(chars), 16);
            i++;
            result[i/2] = (byte) b;
        }

        return result;
    }

    static PublicKey convertPublicKey(final String stringPublicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyFactory ks = KeyFactory.getInstance("DH");
        return ks.generatePublic(new X509EncodedKeySpec(fromStringToByte(stringPublicKey)));
    }

    static PrivateKey convertPrivateKey(final String stringPrivateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyFactory ks = KeyFactory.getInstance("DH");
        return ks.generatePrivate(new PKCS8EncodedKeySpec(fromStringToByte(stringPrivateKey)));
    }

    static byte[] shortenSecretKey(final byte[] longKey) {

        try {

            // Use 8 bytes (64 bits) for DES, 6 bytes (48 bits) for Blowfish
            final byte[] shortenedKey = new byte[8];

            System.arraycopy(longKey, 0, shortenedKey, 0, shortenedKey.length);

            return shortenedKey;

            // Below lines can be more secure
            // final SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            // final DESKeySpec       desSpec    = new DESKeySpec(longKey);
            //
            // return keyFactory.generateSecret(desSpec).getEncoded();
        } catch (Exception e) {
            System.out.println("[ShortenKey] exception: " + e.toString());
            e.printStackTrace();
        }

        return null;
    }
}

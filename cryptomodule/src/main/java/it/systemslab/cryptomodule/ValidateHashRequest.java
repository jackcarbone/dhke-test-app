package it.systemslab.cryptomodule;

public class ValidateHashRequest {

  private String hash;
  private HashablePrivateData privateData;

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  public HashablePrivateData getPrivateData() {
    return privateData;
  }

  public void setPrivateData(HashablePrivateData privateData) {
    this.privateData = privateData;
  }

  public boolean validateRequestObject() {
    return hash != null && privateData.validateRequestObject();
  }

  @Override
  public String toString() {
    return "ValidateHashRequest{" +
        "hash='" + hash + '\'' +
        ", privateData=" + privateData +
        '}';
  }
}
